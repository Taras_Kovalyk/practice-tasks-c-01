﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MyRectangle
{
    struct Point
    {
        public double X { get; set; }
        public double Y { get; set; }
        public Point(double x, double y)
        {
            X = x;
            Y = y;
        }
    }

    class Rectangle
    {
        private Point _leftTop;
        private Point _rightBottom;


        public double LeftTopX
        {
            get
            {
                return _leftTop.X;
            }
            private set
            {
                _leftTop.X = value;
            }
        }

        public double LeftTopY
        {
            get
            {
                return _leftTop.Y;
            }
            private set
            {
                _leftTop.Y = value;
            }
        }

        public double RightBottomX
        {
            get
            {
                return _rightBottom.X;
            }
            private set
            {
                _rightBottom.X = value;
            }
        }

        public double RightBottomY
        {
            get
            {
                return _rightBottom.Y;
            }
            private set
            {
                _rightBottom.Y = value;
            }
        }
        public Rectangle()
        {
        }

        public Rectangle(Point leftTop, Point rightBottom)
        {
            _leftTop.X = leftTop.X;
            _leftTop.Y = leftTop.Y;
            RightBottomX = rightBottom.X;
            RightBottomY = rightBottom.Y;
        }

        public void Move(double xAxis, double yAxis)
        {
            RightBottomY += yAxis;
            LeftTopY += yAxis;

            RightBottomX += xAxis;
            LeftTopX += xAxis;
        }

        public void Resize(double resizeValue)
        {
            resizeValue = resizeValue / 2;

            LeftTopX += resizeValue;
            LeftTopY += resizeValue;
            RightBottomX += resizeValue;
            RightBottomY += resizeValue;
        }

        public static Rectangle CreateSmallestRectangle(Rectangle first, Rectangle second)
        {
            List<double> listOfX = new List<double>() { first.RightBottomX, first.LeftTopX, second.LeftTopX, second.RightBottomX };
            List<double> listOfY = new List<double>() { first.RightBottomY, first.LeftTopY, second.LeftTopY, second.RightBottomY };

            double minX = listOfX.Min();
            double minY = listOfY.Min();
            double maxX = listOfX.Max();
            double maxY = listOfY.Max();

            return new Rectangle(new Point(minX, maxY), new Point(maxX, minY));
        }

        public static Rectangle Intersect(Rectangle first, Rectangle second)
        {
            double maxLeftTopX = Math.Max(first.LeftTopX, second.LeftTopX);
            double minRightBottomX = Math.Min(first.RightBottomX, second.RightBottomX);
            double maxRightBottomY = Math.Max(first.RightBottomY, second.RightBottomY);
            double minLeftTopY = Math.Min(first.LeftTopY, second.LeftTopY);

            if (minRightBottomX >= maxLeftTopX && minLeftTopY >= maxRightBottomY)
            {
                return new Rectangle(new Point(maxLeftTopX, maxRightBottomY + (minLeftTopY - maxRightBottomY)), new Point(maxLeftTopX + (minRightBottomX - maxLeftTopX), maxRightBottomY));
            }

            return null;
        }

        public override string ToString()
        {
            return string.Format($"Left top: ({LeftTopX};{LeftTopY}) right bottom: ({RightBottomX};{RightBottomY})");
        }

        public override bool Equals(object obj)
        {
            if (obj == null || GetType() != obj.GetType())
            {
                return false;
            }
            Rectangle r = (Rectangle)obj;
            if (LeftTopX == r.LeftTopX && LeftTopY == r.LeftTopY && RightBottomX == r.RightBottomX && RightBottomY == r.RightBottomY)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public override int GetHashCode()
        {
            return ToString().GetHashCode();
        }
    }
    class Program
    {
        static void Main(string[] args)
        {
            try
            {

                Rectangle rectangle1 = new Rectangle(new Point(2, 4), new Point(4, 2));
                Rectangle rectangle2 = new Rectangle(new Point(4, 4), new Point(7, 2));
                Rectangle smallestRectangle = new Rectangle();
                Rectangle intersected = new Rectangle();

                Console.WriteLine("Rectangle 1:");
                Console.WriteLine(rectangle1 + "\n");

                Console.WriteLine("Rectangle 2:");
                Console.WriteLine(rectangle2 + "\n\n");

                double moveXAsix = 2;
                double moveYAsix = 3;

                Console.WriteLine($"Move rectangle 1 on x axis - {moveXAsix} ,on y axis - {moveYAsix}");
                rectangle1.Move(moveXAsix, moveYAsix);
                Console.WriteLine("Rectangle :");
                Console.WriteLine(rectangle1 + "\n\n");

                Console.WriteLine("Resize rectangle 1 on value - 3");
                rectangle1.Resize(3);
                Console.WriteLine("Rectangle 1:");
                Console.WriteLine(rectangle1 + "\n\n");

                rectangle1 = new Rectangle(new Point(2, 4), new Point(4, 2));
                rectangle2 = new Rectangle(new Point(3, 3), new Point(6, 1));

                Console.WriteLine("Create smallest rectangle and rectangle on intersection");
                Console.WriteLine("Rectangle 1:");
                Console.WriteLine(rectangle1.ToString() + "\n");
                Console.WriteLine("Rectangle 2:");
                Console.WriteLine(rectangle2.ToString() + "\n");

                smallestRectangle = Rectangle.CreateSmallestRectangle(rectangle1, rectangle2);
                intersected = Rectangle.Intersect(rectangle1, rectangle2);
                Console.WriteLine("Smallest rectangle:");
                Console.WriteLine(smallestRectangle + "\n");
                Console.WriteLine("Rectangle on instersection:");
                Console.WriteLine(intersected + "\n");
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
           
        }
    }
}
