﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MyVector
{
    class Vector
    {
        private Array _array;

        public int Length
        {
            get
            {
                return _array.GetLength(0);

            }
        }

        public int UpperBound
        {
            get
            {
                return _array.GetUpperBound(0);
            }
        }

        public int LowerBound
        {
            get
            {
                return _array.GetLowerBound(0);
            }
        }

        public Vector()
        {
            int[] lengths = { 5 };
            int[] lowerBounds = { 0 };
            _array = Array.CreateInstance(typeof(int), lengths, lowerBounds);
        }

        public Vector(int lowerBound, int upperBound)
        {
            if (upperBound < lowerBound)
            {
                throw new Exception("Invalid upper bound");
            }
            int length = upperBound - lowerBound;
            int[] lengths = { length };
            int[] lowerBounds = { lowerBound };
            _array = Array.CreateInstance(typeof(int), lengths, lowerBounds);
        }

        public void RandomFill(Random random)
        {
            for (int i = LowerBound; i < UpperBound; i++)
            {
                _array.SetValue(random.Next(-10, 10), i);
            }
        }

        public void Show()
        {
            for (int i = LowerBound; i < UpperBound; i++)
            {
                Console.Write(_array.GetValue(i) + " ");
            }
            Console.WriteLine();
        }

        public int this[int index]
        {
            get
            {
                return (int)_array.GetValue(index);
            }
            set
            {
                _array.SetValue(value, index);
            }
        }

        public static Vector operator +(Vector vector1, Vector vector2)
        {
            if (vector1.UpperBound != vector2.UpperBound || vector1.LowerBound != vector2.LowerBound)
            {
                throw new Exception("Vectors have different size");
            }
            int lowerBound = vector1.LowerBound;
            int upperBound = vector1.UpperBound;
            Vector resultVector = new Vector(lowerBound, upperBound);

            for (int i = resultVector.LowerBound; i < resultVector.UpperBound; i++)
            {
                resultVector[i] = vector1[i] + vector2[i];
            }

            return resultVector;
        }

        public static Vector operator -(Vector vector1, Vector vector2)
        {
            if (vector1.UpperBound != vector2.UpperBound || vector1.LowerBound != vector2.LowerBound)
            {
                throw new Exception("Vectors have different size");
            }
            int lowerBound = vector1.LowerBound;
            int upperBound = vector1.UpperBound;
            Vector resultVector = new Vector(lowerBound, upperBound);

            for (int i = resultVector.LowerBound; i < resultVector.UpperBound; i++)
            {
                resultVector[i] = vector1[i] - vector2[i];
            }

            return resultVector;
        }

        public static Vector operator *(Vector vector1, int number)
        {
            int lowerBound = vector1.LowerBound;
            int upperBound = vector1.UpperBound;
            Vector resultVector = new Vector(lowerBound, upperBound);

            for (int i = resultVector.LowerBound; i < resultVector.UpperBound; i++)
            {
                resultVector[i] = vector1[i] * number;
            }

            return resultVector;
        }

        public static Vector operator *(int number, Vector vector1)
        {
            int lowerBound = vector1.LowerBound;
            int upperBound = vector1.UpperBound;
            Vector resultVector = new Vector(lowerBound, upperBound);
            return resultVector * number;
        }

        public static bool operator ==(Vector vector1, Vector vector2)
        {
            return vector1.Equals(vector2);
        }

        public static bool operator !=(Vector vector1, Vector vector2)
        {
            return !(vector1 == vector2);
        }

        public override bool Equals(object obj)
        {
            if (obj == null || GetType() != obj.GetType())
            {
                return false;
            }

            Vector vector = (Vector)obj;
            if (LowerBound != vector.LowerBound || UpperBound != vector.UpperBound)
            {
                return false;
            }

            for (int i = LowerBound; i < UpperBound; i++)
            {
                if ((int)_array.GetValue(i) != vector[i])
                {
                    return false;
                }
            }

            return true;
        }

        public override int GetHashCode()
        {
            return ToString().GetHashCode();
        }

        public override string ToString()
        {
            return string.Format($"Type: Vector, Length: {Length}");
        }
    }
    class Program
    {
        static void Main(string[] args)
        {
            try
            {
                Vector vector = new Vector(-4, 2);
                Vector vector1 = new Vector();
                Vector vector2 = new Vector();
                Vector resultVector = new Vector();
                Random random = new Random();

                Console.WriteLine("Vector created by default constructor.");
                Console.WriteLine($"Lower bound = {vector1.LowerBound}, upper bound = {vector1.UpperBound}");
                Console.WriteLine();
                Console.WriteLine();

                Console.WriteLine("Vector created by paremetrized constructor with  lower bound -4, upper bound 2.");
                Console.WriteLine($"Lower bound = {vector.LowerBound},upper bound = {vector.UpperBound}");
                Console.WriteLine();
                Console.WriteLine();
                
                vector1.RandomFill(random);
                Console.WriteLine("vector 1:");
                vector1.Show();
                Console.WriteLine();
                Console.WriteLine();
                
                vector2.RandomFill(random);
                Console.WriteLine("vector 2:");
                vector2.Show();
                Console.WriteLine();
                Console.WriteLine();
                
                resultVector = vector1 + vector2;
                Console.WriteLine("vector 1 + vector 2");
                resultVector.Show();
                Console.WriteLine();
                Console.WriteLine();
                
                resultVector = vector1 - vector2;
                Console.WriteLine("vector 1 - vector 2");
                resultVector.Show();
                Console.WriteLine();
                Console.WriteLine();
                
                resultVector = vector1 * 5;
                Console.WriteLine("vector 1 multiplied by 5");
                resultVector.Show();
                Console.WriteLine();
                Console.WriteLine();
                
                Console.WriteLine("If vector 1 = vector 2 ?");
                Console.WriteLine(vector1 == vector2);
                Console.WriteLine();
            }

            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }   
        }
    }
}
