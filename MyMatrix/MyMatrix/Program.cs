﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MyMatrix
{
    class Matrix
    {
        private double[,] _matrix;
        private int _rowsCount;
        private int _columnsCount;

        public int Length
        {
            get
            {
                return _rowsCount * _columnsCount;
            }
        }
        public int RowsCount
        {
            get
            {
                return _rowsCount;
            }
            set
            {
                _rowsCount = value;
            }
        }

        public int ColumnsCount
        {
            get
            {
                return _columnsCount;
            }
            set
            {
                _columnsCount = value;
            }
        }

        public Matrix()
        {
            _rowsCount = 3;
            _columnsCount = 4;
            _matrix = new double[RowsCount, ColumnsCount];
        }

        public Matrix(int rowsCount, int columnsCount)
        {
            _rowsCount = rowsCount;
            _columnsCount = columnsCount;
            _matrix = new double[RowsCount, ColumnsCount];
        }

        public Matrix(int rowsCount, int columnsCount, double[,] twoDimensionalArray)
        {
            RowsCount = rowsCount;
            ColumnsCount = columnsCount;
            _matrix = (double[,])twoDimensionalArray.Clone();
        }

        public double this[int i, int j]
        {
            get
            {
                return _matrix[i, j];
            }
            set
            {
                _matrix[i, j] = value;
            }
        }

        public static Matrix operator +(Matrix matrix1, Matrix matrix2)
        {
            if (matrix1.RowsCount != matrix2.RowsCount || matrix1.ColumnsCount != matrix2.ColumnsCount)
            {
                throw new Exception("Incorect sizes of matrixes");
            }
            int rowsCount = matrix1.RowsCount;
            int columnsCount = matrix1.ColumnsCount;
            Matrix resMatrix = new Matrix(rowsCount, columnsCount);
            for (int i = 0; i < rowsCount; i++)
            {
                for (int j = 0; j < columnsCount; j++)
                {
                    resMatrix[i, j] = matrix1[i, j] + matrix2[i, j];
                }
            }
            return resMatrix;
        }

        public static Matrix operator -(Matrix matrix1, Matrix matrix2)
        {
            if (matrix1.RowsCount != matrix2.RowsCount || matrix1.ColumnsCount != matrix2.ColumnsCount)
            {
                throw new Exception("Incorect sizes of matrixes .!. vam");
            }
            int rowsCount = matrix1.RowsCount;
            int columnsCount = matrix1.ColumnsCount;
            Matrix resMatrix = new Matrix(rowsCount, columnsCount);
            for (int i = 0; i < rowsCount; i++)
            {
                for (int j = 0; j < columnsCount; j++)
                {
                    resMatrix[i, j] = matrix1[i, j] - matrix2[i, j];
                }
            }
            return resMatrix;
        }

        public static Matrix operator *(Matrix matrix1, Matrix matrix2)
        {
            if (matrix1.RowsCount != matrix2.RowsCount || matrix1.ColumnsCount != matrix2.ColumnsCount)
            {
                throw new Exception("Incorect sizes of matrixes .!. vam");
            }
            int rowsCount = matrix1.RowsCount;
            int columnsCount = matrix1.ColumnsCount;
            Matrix resMatrix = new Matrix(rowsCount, columnsCount);
            for (int i = 0; i < rowsCount; i++)
            {
                for (int j = 0; j < columnsCount; j++)
                {
                    for (int k = 0; k < rowsCount; k++)
                    {
                        resMatrix[i, j] += matrix1[i, k] * matrix2[k, j];
                    }
                }
            }
            return resMatrix;
        }

        public double GetMinorOfOrder(int order)
        {
            if (!this.IsSquare)
                throw new ArgumentException("Matrix must be square");

            double[,] subMatr = new double[order, order];

            for (int i = 0; i < order; i++)
            {
                for (int j = 0; j < order; j++)
                {
                    subMatr[i, j] = this._matrix[i, j];
                }
            }

            return GetDeterminantValue(subMatr);
        }

        public double GetSupplementaryMinor(int order)
        {
            if (!this.IsSquare)
                throw new ArgumentException("Matrix must be square");

            int subMatrixOrder = this.ColumnsCount - order;

            double[,] subMatrix = new double[subMatrixOrder, subMatrixOrder];

            int subMatrixRowNumber = 0;
            for (int i = subMatrixOrder; i >= order; i--)
            {
                int subMatrixColumnNumber = 0;
                for (int j = subMatrixOrder; j >= order; j--)
                {
                    subMatrix[subMatrixRowNumber, subMatrixColumnNumber] = this._matrix[i, j];
                    subMatrixColumnNumber++;
                }
                subMatrixRowNumber++;
            }

            return GetDeterminantValue(subMatrix);
        }

        public double GetMinorOfElement(int iCoordinate, int jCoordinate)
        {
            if (!this.IsSquare)
                throw new ArgumentException("Matrix must be square");

            int subMatrixOrder = this.ColumnsCount - 1;

            double[,] subMatrix = new double[subMatrixOrder, subMatrixOrder];

            int subMatrixRowNumber = 0;
            for (int i = 0; i < this.ColumnsCount; i++)
            {
                if (i == iCoordinate && i < this.ColumnsCount - 1)
                    i++;

                int subMatrixColumnNumber = 0;
                for (int j = 0; j < this.RowsCount; j++)
                {
                    if (j == jCoordinate && j < this.ColumnsCount - 1)
                        j++;

                    subMatrix[subMatrixRowNumber, subMatrixColumnNumber] = this._matrix[i, j];
                    subMatrixColumnNumber++;

                }
                subMatrixRowNumber++;

            }

            return GetDeterminantValue(subMatrix);
        }

        private static double GetDeterminantValue(double[,] matrix)
        {

            int order = int.Parse(Math.Sqrt(matrix.Length).ToString());

            if (order == 2)
            {
                return matrix[0, 0] * matrix[1, 1] - matrix[0, 1] * matrix[1, 0];
            }
            else if (order > 2)
            {
                double determinantValue = 0;

                for (int i = 0; i < order; i++)
                {
                    double[,] subMatrix = CreateSubMatrix(matrix, 0, i);
                    determinantValue += ElementSign(0, i) * matrix[0, i] * GetDeterminantValue(subMatrix);
                }
                return determinantValue;
            }
            else
            {
                return matrix[0, 0];
            }
        }

        private static double[,] CreateSubMatrix(double[,] matrix, int rowsCount, int columnsCount)
        {
            int order = int.Parse(Math.Sqrt(matrix.Length).ToString());

            double[,] subMatrix = new double[order - 1, order - 1];

            int subMatrixRowNumber = 0;
            int subMatrixColumnNumber;
            for (int rowNumber = 0; rowNumber < order; rowNumber++, subMatrixRowNumber++)
            {
                if (rowNumber != rowsCount)
                {
                    subMatrixColumnNumber = 0;
                    for (int columnNumber = 0; columnNumber < order; columnNumber++)
                    {
                        if (columnNumber != columnsCount)
                        {
                            subMatrix[subMatrixRowNumber, subMatrixColumnNumber] = matrix[rowNumber, columnNumber];
                            subMatrixColumnNumber++;
                        }
                    }
                }
                else
                {
                    subMatrixRowNumber--;
                }
            }
            return subMatrix;
        }

        private static double ElementSign(int rowIndex, int columnIndex)
        {
            return Math.Pow(-1, rowIndex + columnIndex);
        }

        public bool IsSquare
        {
            get
            {
                if (RowsCount == ColumnsCount)
                    return true;
                else
                    return false;
            }
        }
        public void Show()
        {
            for (int i = 0; i < RowsCount; i++)
            {
                for (int j = 0; j < ColumnsCount; j++)
                {
                    Console.Write(_matrix[i, j] + " ");
                }
                Console.WriteLine();
            }
        }

        public void RandomFill(Random r)
        {
            for (int i = 0; i < RowsCount; i++)
            {
                for (int j = 0; j < ColumnsCount; j++)
                {
                    _matrix[i, j] = r.Next(0, 10);
                }
            }
        }
    }
    class Program
    {
        static void Main(string[] args)
        {
            try
            {  

                Matrix matrix1 = new Matrix(3, 3);
                Matrix matrix2 = new Matrix(3, 3);
                Matrix resultMatrix = new Matrix(3, 3);
                Random random = new Random();

                Console.WriteLine("Matrix 1");
                matrix1.RandomFill(random);
                matrix1.Show();
                Console.WriteLine();

                Console.WriteLine("Matrix 2");
                matrix2.RandomFill(random);
                matrix2.Show();
                Console.WriteLine();

                Console.WriteLine("Matrix 1 + Matrix 2");
                resultMatrix = matrix1 + matrix2;
                resultMatrix.Show();
                Console.WriteLine();

                Console.WriteLine("Matrix 1 - Matrix 2");
                resultMatrix = matrix1 - matrix2;
                resultMatrix.Show();
                Console.WriteLine();

                Console.WriteLine("Matrix 1 * Matrix 2");
                resultMatrix = matrix1 * matrix2;
                resultMatrix.Show();
                Console.WriteLine();

                int order = 2;
                double minor = matrix1.GetMinorOfOrder(order);
                Console.WriteLine($"Minor of {order} order in matrix 1 is: {minor}");
                Console.WriteLine();

                int order1 = 1;
                double supMinor = matrix1.GetSupplementaryMinor(order1);
                Console.WriteLine($"Supplementary minor of {order1} order in matrix 1 is: {supMinor}");
                Console.WriteLine();
                
                int row = 1;
                int column = 0;
                double elMinor = matrix1.GetMinorOfElement(row,column);
                Console.WriteLine($"Minor of el {matrix1[row,column]}, row = {row}, column = {column} in matrix 1 is: {elMinor}");
                Console.WriteLine();

            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
        }
    }
}
